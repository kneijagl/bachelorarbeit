\section{Abelsche Prägarben auf einem Situs}

\begin{festsetzung}
	Künftig bezeichne $\mathcal{T}$ stets einen Situs, $\mathcal{P}$ die Kategorie der abelschen Prägarben auf $\mathcal{T}$ und $\mathcal{S}$ die Kategorie der abelschen Garben auf $\mathcal{T}$. Per Definition ist $\S$ eine volle Unterkategorie von $\P$ und wir werden mit $\iota : \S \rightarrow \P$ die kanonische Inklusion bezeichnen.
\end{festsetzung}

\subsection{Die Kategorie der abelschen Prägarben}

\begin{proposition}\label{PIstAbelscheKategorie}
	\quad
	\begin{enumerate}
		\item[(a)] $\mathcal{P}$ ist eine abelsche Kategorie mit der Eigenschaft (Ab5) und besitzt Generatoren.
		
		\item[(b)] Eine Sequenz $F' \rightarrow F \rightarrow F''$ abelscher Prägarben ist exakt in $\mathcal{P}$ genau dann, wenn für alle $U \in \mathcal{T}$ die Sequenz $F'(U) \rightarrow F(U) \rightarrow F''(U)$ exakt in $\boldsymbol{Ab}$ ist.
	\end{enumerate}
\end{proposition}

\begin{proof}[Beweis (Übernommen aus \cite{tamme1975einfuhrung}, 1.2.1.1)]
	Die Aussagen der Proposition ergeben sich direkt als Anwendung der allgemeineren Propositionen~\ref{FunktorkategorieInAbelscheKategorieIstAbelsch}, \ref{(Ab5)ÜbertragungAufFunktorkategorie} und \ref{GeneratorenÜbertragungAufFunktorkategorie} auf die Kategorien $\mathcal{T}^{o}$ und $\boldsymbol{Ab}$.
\end{proof}

\begin{bemerkung}[Übernommen aus \cite{tamme1975einfuhrung}, 1.2.1.3.]\label{GeneratorenInKategorieDerAbelschenPrägarben}
	Da $\mathbb{Z}$ ein Generator der Kategorie $\boldsymbol{Ab}$ ist, kann man dem Beweis von Proposition~\ref{GeneratorenÜbertragungAufFunktorkategorie} entnehmen, dass die Familie $(Z_{U})_{U \in \mathcal{T}}$ der abelschen Prägarben definiert durch
	\[
		Z_{U}(V):= \bigoplus \limits_{Hom(V,U)} \mathbb{Z} \qquad , \qquad V \in \mathcal{T}
	\]
	eine Familie von Generatoren für $\mathcal{P}$ bildet. Außerdem kann man dem Beweis entnehmen, dass für jede abelsche Prägarbe $F$ ein in $F$ funktorieller Isomorphismus
	\[
		F(U) \cong Hom(\mathbb{Z}, F(U)) \cong Hom(Z_{U}, F)
	\]
	existiert, d.h. für jedes $U \in \mathcal{T}$ wird der \textit{Schnittfunktor} 
	\[
		\Gamma_{U} : \mathcal{P} \rightarrow \boldsymbol{Ab} , F \mapsto F(U)
	\]
	durch $Z_{U}$ dargestellt.
\end{bemerkung}

Angesichts Proposition~\ref{PIstAbelscheKategorie} sind die Voraussetzungen von Theorem~\ref{(Ab5)+GeneratorenFolgtGenügendVieleInjektiveObjekte} erfüllt und man erhält:

\begin{korollar}\label{PHatGenügendVieleInjektiveObjekte}
	Die Kategorie $\mathcal{P}$ besitzt genügend viele injektive Objekte.
\end{korollar}

Aufgrund von Korollar~\ref{PHatGenügendVieleInjektiveObjekte} folgt mit Theorem~\ref{ExistenzDerRechtsableitung}:

\begin{korollar}
	Zu jedem linksexakten Funktor $F : \mathcal{P} \rightarrow \mathcal{C}$ von $\mathcal{P}$ in eine abelsche Kategorie $\mathcal{C}$ existiert die Rechtsableitung $(R^{i}F)_{i \geq 0}$.
\end{korollar}

\begin{beispiel}[Eigenleistung]
	Der Schnittfunktor $\Gamma_{U} : \mathcal{P} \rightarrow \boldsymbol{Ab}$ ist nach Proposition~\ref{PIstAbelscheKategorie} (b) für jedes $U \in \mathcal{T}$ exakt, sodass $R^{i}\Gamma_{U} = 0$ für $i \geq 1$ gilt.
\end{beispiel}

\begin{festsetzung}
	Ist $F: \mathcal{I} \rightarrow \mathcal{P}$ ein Funktor, so schreiben wir $F_{i}$ statt $F(i)$ für $i \in \mathcal{I}$ und für das darstellende Objekt von $\varinjlim F$, welches nach Proposition~\ref{InduktiverLimesRechtsexakterFunktor} existiert, statt $\underset{i \in \mathcal{I}}{\varinjlim} F$ auch $\varinjlim F_{i}$.
\end{festsetzung}

\begin{proposition}\label{InduktiverLimesVonFunktorenInPSindDarstellbar}
	\begin{enumerate}
		\item[(a)] Für jeden Funktor $F : \mathcal{I} \rightarrow \mathcal{P}$ ist der induktive Limes $\varinjlim F$ darstellbar und es gilt $(\varinjlim F_{i})(U) = \varinjlim (F_{i}(U))$, wobei letzteres den induktiven Limes von $\Gamma_{U} \circ F$ bezeichnen soll. Außerdem ist der Funktor $\varinjlim : Hom(\mathcal{I}, \mathcal{P}) \rightarrow \mathcal{P}$ additiv und rechtsexakt.
		
		\item[(b)] Ist $\mathcal{I}$ pseudofiltrierend, so ist der Funktor der Funktor $\varinjlim : Hom(\mathcal{I}, \mathcal{P}) \rightarrow \mathcal{P}$ exakt.
	\end{enumerate}
\end{proposition}

\begin{proof}[Beweis (Übernommen aus \cite{tamme1975einfuhrung}, 1.2.1.4.)]
	Man überprüft leicht, dass die abelsche Prägarbe $U \mapsto \varinjlim (F_{i}(U))$ zusammen mit den durch $F_{i}(U) \rightarrow \varinjlim (F_{i}(U))$ (vgl. Beweis Prop.~\ref{InduktiverLimesRechtsexakterFunktor}) gegebenen Prägarbenmorphismen den Funktor $\varinjlim F : \mathcal{P} \rightarrow \boldsymbol{Set}$ darstellt. Die weiteren Behauptungen folgen aus Proposition~\ref{InduktiverLimesRechtsexakterFunktor} und Proposition~\ref{InduktiverLimesExakterFunktor}.
\end{proof}




\subsection{\v{C}ech-Kohomologie}

\begin{definition}
	Für jede Überdeckung $(U_{i} \rightarrow U)_{i \in I}$ in $\mathcal{T}$ definieren wir den additiven Funktor
	\[
		H^{0}((U_{i} \rightarrow U)_{i \in I}, \_) : \mathcal{P} \rightarrow \boldsymbol{Ab}
	\]
	durch
	\[
		H^{0}((U_{i} \rightarrow U)_{i \in I}, F) := Ker(
		\shorthandoff{"}
		\begin{tikzcd}
		\prod \limits_{i \in I} F(U_{i}) \arrow[r, shift left= 1.25] \arrow[r, shift right= 1]
		& \prod \limits_{i,j \in I} F(U_{i} \times_{U} U_{j})
		\end{tikzcd}
		)
	\]
	für jede abelsche Prägarbe $F\in \P$. Wie man dem Schlangenlemma und Bemerkung~\ref{KernDifferenzkernZusammenhangInAbelschenKategorien} entnehmen kann, ist der soeben definierte Funktor sogar linksexakt, im Allgemeinen aber nicht exakt. Es existiert also seine Rechtsableitung und wir definieren für jedes $F \in \mathcal{P}$ und jedes $n \geq 0$
	\[
		H^{n}((U_{i} \rightarrow U)_{i \in I}, F):=R^{n}H^{0}((U_{i} \rightarrow U)_{i \in I}, \_) (F)
	\]
	als die \textit{$(U_{i} \rightarrow U)_{i \in I}$ assoziierte n-te \v{C}ech-Kohomologiegruppe mit Werten in $F$}.
\end{definition}

%\begin{motivation}
%	Ist $F \in \mathcal{S}$ eine abelsche Garbe auf $\mathcal{T}$, so ist per Definition das Diagramm
%	\begin{center}
%		\shorthandoff{"}
%		\begin{tikzcd}
%		F(U) \arrow[r]
%		& \prod \limits_{i \in I} F(U_{i}) \arrow[r, shift left=1.25] \arrow[r, shift right=1.25]
%		& \prod \limits_{i,j \in I} F(U_{i} \times_{U} U_{j})
%		\end{tikzcd}
%	\end{center}
%	exakt, d.h. es gilt $H^{0}((U_{i} \rightarrow U)_{i \in I}, F) \cong F(U)$.
% 	...
%\end{motivation}

Zur Bestimmung der \v{C}ech-Kohomologiegruppen $H^{n}((U_{i} \rightarrow U)_{i \in I}, F)$ wird uns der im Folgenden definierte Komplex der \v{C}ech'schen Koketten von Nutzen sein.

\begin{definition}\label{CechscheKokettenDef}
	Zu jeder Überdeckung $(U_{i} \rightarrow U)_{i \in I}$ in $\mathcal{T}$ und jeder abelschen Prägarbe $F \in \mathcal{P}$ definieren wir für alle $n \geq 0$ die \textit{Gruppe der n-Koketten zur Überdeckung $(U_{i} \rightarrow U)_{i \in I}$ mit Werten in $F$} als
	\[
		C^{n}((U_{i} \rightarrow U)_{i \in I}, F) := \prod \limits_{(i_{0}, ..., i_{n}) \in I^{n+1}} F(U_{i_{0}} \times_{U} ... \times_{U} U_{i_{n}}) \textrm{ .}
	\]
	Weiter definieren wir für alle $n \geq 0$ den \textit{Korandoperator}
	\[
		d^{n} : C^{n}((U_{i} \rightarrow U)_{i \in I}, F) \rightarrow C^{n+1}((U_{i} \rightarrow U)_{i \in I}, F)
	\]
	als den eindeutigen Morphismus, für den
	\[
		pr^{C^{n+1}}_{(i_{0}, ..., i_{n+1})} \circ d^{n} = \sum_{\nu = 0}^{n+1} (-1)^{\nu} F(\hat{\nu}) \circ pr^{C^{n}}_{(i_{0}, ..., \hat{i_{\nu}}, ..., i_{n+1})}
	\]
	für alle Tupel $(i_{0}, ..., i_{n+1}) \in I^{n+2}$ gilt, wobei hier
	\[
		\hat{\nu} : U_{i_{0}} \times_{U} ... \times_{U} U_{i_{n+1}} \rightarrow U_{i_{0}} \times_{U} ... \times_{U} \widehat{U_{i_{\nu}}} \times_{U} ... \times_{U} U_{i_{n+1}}
	\]
	die natürliche Projektion auf das durch Weglassen des Faktors $U_{i_{\nu}}$ entstehende Faserprodukt bezeichnet.
\end{definition}

\begin{proposition}
	Die in Definition~\ref{CechscheKokettenDef} definierten Gruppen der Koketten zu einer Überdeckung $(U_{i} \rightarrow U)_{i \in I}$ mit Werten in einer abelschen Prägarbe $F$ bilden zusammen mit den Korandoperatoren einen Kokettenkomplex $C^{*}((U_{i} \rightarrow U)_{i \in I}, F)$, den sogenannten \v{C}ech'schen Kokettenkomplex.
\end{proposition}

\begin{proof}[Beweis (Eigenleistung)]
	Für ein beliebiges $n \geq 0$ gilt $d^{n+1} \circ d^{n}= 0$ genau dann, wenn für alle $(i_{0}, ... i_{n+2}) \in I^{n+1} : pr_{(i_{0}, ... i_{n+2})} \circ d^{n+1} \circ d^{n}= 0$ gilt. Für alle $(i_{0}, ... i_{n+2}) \in I^{n+1}$ gilt :
	\begin{align*}
		pr_{(i_{0}, ... i_{n+2})} \circ d^{n+1} \circ d^{n} & =
		\sum_{\nu = 0}^{n+2} (-1)^{\nu} F(\hat{\nu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\nu}}, ..., i_{n+1})} \circ d^{n} =\\
		= \sum_{\nu = 0}^{n+2} (-1)^{\nu} F(\hat{\nu}) & \circ \left( \sum_{\mu = 0}^{\nu - 1} (-1)^{\mu} F(\hat{\mu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\mu}}, ..., \hat{i_{\nu}}, ..., i_{n+1})} + \right. \\
		& \left. + \sum_{\mu = \nu + 1}^{n+2} (-1)^{\mu - 1} F(\hat{\mu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\nu}}, ..., \hat{i_{\mu}}, ..., i_{n+1})} \right) = \\
		= \sum_{\nu = 1}^{n+2} \sum_{\mu = 0}^{\nu - 1} (-1)^{\nu + \mu} & F(\hat{\mu} \circ \hat{\nu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\mu}}, ..., \hat{i_{\nu}}, ..., i_{n+1})} - \\
		& - \sum_{\mu = 0}^{n+1} \sum_{\nu = \mu +1}^{n + 2} (-1)^{\mu + \nu} F(\hat{\nu} \circ \hat{\mu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\mu}}, ..., \hat{i_{\nu}}, ..., i_{n+1})} = \\
		= \sum_{\nu = 1}^{n+2} \sum_{\mu = 0}^{\nu - 1} (-1)^{\nu + \mu} & F(\hat{\mu} \circ \hat{\nu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\mu}}, ..., \hat{i_{\nu}}, ..., i_{n+1})} - \\
		& - \sum_{\nu = 1}^{n+2} \sum_{\mu = 0}^{\nu - 1} (-1)^{\mu + \nu} F(\hat{\nu} \circ \hat{\mu}) \circ pr^{C^{n+1}}_{(i_{0}, ..., \hat{i_{\mu}}, ..., \hat{i_{\nu}}, ..., i_{n+1})} = 0
	\end{align*}
	Dabei gilt das letzte "`="', da die natürliche Projektion 
	\[
		U_{i_{0}} \times_{U} ... \times_{U} U_{i_{n+2}} \rightarrow U_{i_{0}} \times_{U} ... \times_{U} \widehat{U_{i_{\mu}}} \times_{U} ... \times_{U} \widehat{U_{i_{\nu}}} \times_{U} ... \times_{U} U_{i_{n+2}}
	\]
	dank der universellen Eigenschaft von Faserprodukten eindeutig ist, d.h. es gilt $\hat{\mu} \circ \hat{\nu} = \hat{\nu} \circ \hat{\mu}$.
\end{proof}

\begin{theorem}\label{CechscheKokettenZurBestimmungDerCechschenKohomologiegruppen}
	Für jede abelsche Prägarbe $F$ auf $\mathcal{T}$ identifiziert sich $H^{n}((U_{i} \rightarrow U)_{i \in I}, F)$ kanonisch mit dem n-ten Kohomologieobjekt des \v{C}ech'schen Kokettenkomplexes $C^{*}((U_{i} \rightarrow U)_{i \in I}, F)$.
\end{theorem}

\begin{proof}[Beweisskizze (Basiert auf \cite{tamme1975einfuhrung}, 1.2.2.3.)]
	Wir bezeichnen das $n$-te Kohomologieobjekt $Ker(d^{n})/Im(d^{n-1})$ des \v{C}ech'schen Kokettenkomplexes $C^{*}((U_{i} \rightarrow U)_{i \in I}, F)$ vorübergehend mit $\tilde{H}^{n}((U_{i} \rightarrow U)_{i \in I}, F)$. Ein Morphismus $F \rightarrow G$ von abelschen Prägarben induziert in natürlicher Weise einen Morphismus $C^{*}((U_{i} \rightarrow U)_{i \in I}, F) \rightarrow C^{*}((U_{i} \rightarrow U)_{i \in I}, G)$ von Kokettenkomplexen. Daher erhalten wir einen Funktor
	\[
		C^{*}((U_{i} \rightarrow U)_{i \in I}, \_) : \P \rightarrow Ch^{\bullet}(\C),
	\]
	der jeder Prägarbe $F$ auf $\T$ den \v{C}ech'schen Kokettenkomplex zur Überdeckung $(U_{i} \rightarrow U)_{i \in I}$ mit Werten in $F$ zuweist. Man kann leicht überprüfen, dass dieser Funktor sogar additiv und exakt ist. Aufgrund von Theorem~\ref{VerbindungsmorphismenDerKohomologieobjekteExistenz} bilden die Kohomologieobjekte $\tilde{H}^{n}((U_{i} \rightarrow U)_{i \in I}, F)$ demnach einen exakten $\partial$-Funktor
	\[
		\tilde{H}^{*}((U_{i} \rightarrow U)_{i \in I}, \_) = H^{*}(\_) \circ C^{*}((U_{i} \rightarrow U)_{i \in I}, \_): \P \rightarrow \C.
	\]
	Dieser $\partial$-Funktor setzt den Funktor $H^{0}((U_{i} \rightarrow U)_{i \in I}, \_)$ fort, d.h. es gilt $\tilde{H}^{0}((U_{i} \rightarrow U)_{i \in I}, \_)=H^{0}((U_{i} \rightarrow U)_{i \in I}, \_)$, weshalb im Hinblick auf Definition~\ref{RechtsableitungDef} nur noch die Universalität von $\tilde{H}^{*}((U_{i} \rightarrow U)_{i \in I}, \_)$ zu zeigen bleibt. Hierfür vergleiche man \cite{tamme1975einfuhrung}, 1.2.2.3.
\end{proof}

\begin{definition}
	Sei $U \in \mathcal{T}$ beliebig. Eine \textit{Verfeinerungsabbildung}
	\[
		f : (U'_{j} \rightarrow U)_{j \in J} \rightarrow (U_{i} \rightarrow U)_{i \in I}
	\]
	\textit{von Überdeckungen von $U$} ist eine Abbildung $\epsilon : J \rightarrow I$ der Indexmengen zusammen mit einer Familie $(f_{j})_{j \in J}$ von Morphismen $f_{j} : U'_{j} \rightarrow U_{\epsilon (j)}$, für welche das Diagramm
	\begin{center}
		\shorthandoff{"}
		\begin{tikzcd}[column sep= small]
			U'_{j} \arrow[rr] \arrow[dr]
			&& U_{\epsilon (j)} \arrow[dl] \\
			& U
		\end{tikzcd}
	\end{center}
	für alle $j \in J$ kommutiert, sogenannte Über-$U$-Morphismen. Die Menge der Überdeckungen von $U$ in $\mathcal{T}$ bildet zusammen mit den Verfeinerungsabbildungen als Morphismen die kleine \textit{Kategorie} $J_{U}$ \textit{der Überdeckungen von $U$} in $\mathcal{T}$.
\end{definition}

\begin{proposition}\label{VerfeinerungsabbildungInduziertEindeutigenMorphismusZwischenCechKohomologiegruppen}
	Für jede Verfeinerungsabbildung $f : (U'_{j} \rightarrow U)_{j \in J} \rightarrow (U_{i} \rightarrow U)_{i \in I}$ von Überdeckungen von $U \in \mathcal{T}$ existiert ein kanonischer Morphismus
	\[
		H^{*}(f,\_) : H^{*}((U_{i} \rightarrow U)_{i \in I}, \_) \rightarrow H^{*}((U'_{j} \rightarrow U)_{j \in J}, \_)
	\]
	von $\partial$-Funktoren.
\end{proposition}

\begin{proof}[Beweis (Übernommen aus \cite{tamme1975einfuhrung}, 1.2.2.)]
	Sei $F \in \mathcal{P}$ eine abelsche Prägarbe auf $\mathcal{T}$. Die Verfeinerungsabbildung $f$ induziert in natürlicher Weise Morphismen $f^{0}$ und $f^{1}$, sodass das Diagramm
	\begin{center}
		\shorthandoff{"}
		\begin{tikzcd}
			\prod \limits_{i \in I} F(U_{i}) \arrow[r, shift left= 1.25] \arrow[r, shift right= 1] \arrow[d, "f^{0}"']
			& \prod \limits_{i_{0}, i_{1} \in I} F(U_{i_{0}} \times_{U} U_{i_{1}}) \arrow[d, "f^{1}"] \\
			\prod \limits_{j \in J} F(U'_{j}) \arrow[r, shift left= 1.25] \arrow[r, shift right= 1]
			& \prod \limits_{j_{0}, j_{1} \in J} F(U'_{j_{0}} \times_{U} U'_{j_{1}})
		\end{tikzcd}
	\end{center}
	kommutiert. Dieses Diagramm liefert uns einen in $F$ funktoriellen Morphismus
	\[
		H^{0}(f,F) : H^{0}((U_{i} \rightarrow U)_{i \in I}, F) \rightarrow H^{0}((U'_{j} \rightarrow U)_{j \in J}, F)
	\]
	und dank der Universalität des $\partial$-Funktors $H^{*}((U_{i} \rightarrow U)_{i \in I}, \_)$ lässt sich dieser eindeutig zu dem gewünschten Morphismus von $\partial$-Funktoren fortsetzen.
\end{proof}

\begin{korollar}\label{CechKohomologiegruppenFunktorVonKategorieDerÜberdeckungen}
	Für jede abelsche Prägarbe $F$ auf $\mathcal{T}$ und für jedes $n \geq 0$ existiert ein kontravarianter Funktor
	\[
		H^{n}(\_, F) : J_{U} \rightarrow \boldsymbol{Ab} \quad , \quad (U_{i} \rightarrow U)_{i \in I} \mapsto H^{n}((U_{i} \rightarrow U)_{i \in I}, F)
	\]
	von der Kategorie $J_{U}$ der Überdeckungen von $U \in \mathcal{T}$ in die Kategorie $\boldsymbol{Ab}$ der abelschen Gruppen.
\end{korollar}

\begin{definition}
	Seien $F \in \mathcal{P}$ und $n \geq 0$ beliebig. Fassen wir den kontravarianten Funktor $H^{n}(\_, F)$ aus Korollar~\ref{CechKohomologiegruppenFunktorVonKategorieDerÜberdeckungen} als kovarianten Funktor von $J^{op}_{U}$ in $\boldsymbol{Ab}$ auf, so können wir dank Proposition~\ref{InduktiverLimesRechtsexakterFunktor} durch
	\[
		\check{H}^{n}(U, F) := \underset{(U_{i} \rightarrow U)_{i \in I}}{\varinjlim} H^{n}((U_{i} \rightarrow U)_{i \in I}, F)
	\]
	die \textit{n-te \v{C}ech-Kohomologiegruppe von $U$ mit Werten in $F$} definieren.
\end{definition}

\begin{theorem}\label{CechKohomologiegruppenFunktorURechtsableitung}
	Der Funktor
	\[
		\check{H}^{0}(U, \_) : \mathcal{P} \rightarrow \boldsymbol{Ab} \quad , \quad F \mapsto \check{H}^{0}(U, F)
	\]
	ist linksexakt, additiv und für seine Rechtsableitungen gilt
	\[
		R^{n}(\check{H}^{0}(U, F)) = \check{H}^{n}(U, F)
	\]
	für alle $n \geq 0$.
\end{theorem}

\begin{proof}[Beweis (Ergänzt \cite{tamme1975einfuhrung}, 1.2.2.6.)]
	Zunächst fällt einem vielleicht auf, dass die Funktoren $\check{H}^{n}(U, \_)$ in 
	\[
		\mathcal{P} \xrightarrow{H^{n}(\_ , \_)} Hom(J^{op}_{U}, \boldsymbol{Ab}) \xrightarrow{\varinjlim} \boldsymbol{Ab}
	\]
	faktorisieren. Man kann angesichts der Additivität und Linksexaktheit von $H^{n}((U_{i} \rightarrow U)_{i \in I}, \_)$ für alle $U \in \mathcal{T}$ leicht überprüfen, dass sich beide Eigenschaften auch auf $H^{n}(\_ , \_)$ übertragen. Nehmen wir nun an, die Kategorie $J^{op}_{U}$ wäre pseudofiltrierend, so wäre $\varinjlim$ aufgrund von Theorem~\ref{InduktiverLimesExakterFunktor} exakt und additiv. Die Linksexaktheit und Additivität von $H^{0}(U, \_)$ wäre somit offensichtlich erfüllt und auch der restliche Teil des Beweises würde sich routinemäßig ergeben. Wie man an einfachen Beispielen überprüfen kann, ist die Kategorie $J^{op}_{U}$ jedoch im Allgemeinen nicht pseudofiltrierend.
	Dennoch greifen wir diesen Ansatz auf, denn wir werden im Folgenden die pseudofiltrierende Kategorie $J^{op}_{U}/\geq$ definieren und jeden Funktor der Form $H^{n}(\_, F)$ aus $Hom(J^{op}_{U}, \boldsymbol{Ab})$ kanonisch mit einem Funktor aus $Hom(J^{op}_{U}/\geq, \boldsymbol{Ab})$ identifizieren, sodass die Funktoren $\check{H}^{n}(U, \_)$ auch in 
	\[
		\mathcal{P} \xrightarrow{H^{n}(\_ , \_)} Hom(J^{op}_{U}/\geq, \boldsymbol{Ab}) \xrightarrow{\varinjlim} \boldsymbol{Ab}
	\]
	faktorisieren. Gelingt uns dies, so ist die Linksexaktheit und Additivität von $H^{0}(U, \_)$ analog zu oben gegeben und der restliche Teil des Beweises folgt erneut routinemäßig (vgl. \cite{tamme1975einfuhrung} 1.2.2.6.).
	
\begin{definition}
	Sind $(U_{i} \rightarrow U)_{i \in I}$ und $(U'_{i} \rightarrow U)_{j \in J}$ zwei Überdeckungen von $U \in \mathcal{T}$, so nennen wir $(U'_{j} \rightarrow U)_{j \in J}$ \textit{feiner} als $(U_{i} \rightarrow U)_{i \in I}$ und schreiben $(U'_{j} \rightarrow U)_{j \in J} \geq (U_{i} \rightarrow U)_{i \in I}$, wenn mindestens eine Verfeinerungsabbildung $f : (U'_{j} \rightarrow U)_{j \in J} \rightarrow (U_{i} \rightarrow U)_{i \in I}$ existiert.
\end{definition}

\begin{proposition}
	Die Menge der Überdeckungen von $U$ wird mit der Relation $\geq$ zu einer gerichteten Menge. Wir fassen diese gerichtete Menge wie im Beispiel~\ref{GerichteteMengeAlsFiltrierendeKategorie} als filtrierende Kategorie auf und bezeichnen sie mit $J^{op}_{U}/\geq$.
\end{proposition}

\begin{proof}[Beweis (Übernommen aus \cite{tamme1975einfuhrung}, 1.2.2.6.)]
	Für je zwei Überdeckungen $(U_{i} \rightarrow U)_{i \in I}, (U'_{j} \rightarrow U)_{j \in J} \in J_{U}$ ist hinsichtlich (T1) und (T2) auch
	\[
		(U_{i} \times_{U} U'_{j} \rightarrow U)_{(i,j) \in I \times J}
	\]
	eine Überdeckung von $U$ und die Projektionen der Faserprodukte induzieren kanonische Verfeinerungsabbildungen
	\begin{center}
		\shorthandoff{"}
		\begin{tikzcd}[column sep = small, row sep = small]
			& (U_{i} \rightarrow U)_{i \in I} \\
			(U_{i} \times_{U} U'_{j} \rightarrow U)_{(i,j) \in I \times J} \arrow[ur] \arrow[dr] \\
			& (U'_{j} \rightarrow U)_{j \in J} \hspace{0.3em} .
		\end{tikzcd}
	\end{center}
\end{proof}

\begin{proposition}\label{VonVerfeinerungsabbildungenInduzierterDeltaFunktorIstUnabhängigVonVerfeinerungsabbildung}
	Seien $(U_{i} \rightarrow U)_{i \in I}, (U'_{j} \rightarrow U)_{j \in J} \in J_{U}$ zwei Überdeckungen von $U$ und $f,g : (U'_{j} \rightarrow U)_{j \in J} \rightarrow (U_{i} \rightarrow U)_{i \in I}$ zwei Verfeinerungsabbildungen. Dann stimmen die induzierten Morphismen
	\[
		H^{n}(f,F), H^{n}(g,F) : H^{n}((U_{i} \rightarrow U)_{i \in I}, F) \rightarrow H^{n}((U'_{j} \rightarrow U)_{j \in J}, F)
	\]
	für jede abelsche Prägarbe $F$ und jedes $n \geq 0$ überein.
\end{proposition}

Für den Beweis von Proposition~\ref{VonVerfeinerungsabbildungenInduzierterDeltaFunktorIstUnabhängigVonVerfeinerungsabbildung} vergleiche man \cite{tamme1975einfuhrung} 1.2.2.7.
\\

Infolge von Proposition~\ref{VonVerfeinerungsabbildungenInduzierterDeltaFunktorIstUnabhängigVonVerfeinerungsabbildung} lässt sich jeder Funktor der Form $H^{n}(\_, F)$ aus $Hom(J^{op}_{U}, \boldsymbol{Ab})$ als Funktor aus $Hom(J^{op}_{U}/\geq, \boldsymbol{Ab})$ interpretieren. Man überprüft leicht, dass die Funktoren $\mathcal{P} \xrightarrow{H^{n}(\_ , \_)} Hom(J^{op}_{U}/\geq, \boldsymbol{Ab})$ weiterhin additiv und exakt bleiben und das Diagramm
\begin{center}
	\shorthandoff{"}
	\begin{tikzcd}
		\mathcal{P} \arrow[r, "{H^{n}(\_, \_)}"] \arrow[d, "{H^{n}(\_, \_)}"']
		& Hom(J^{op}_{U}/\geq, \boldsymbol{Ab}) \arrow[d, "\varinjlim"] \\
		Hom(J^{op}_{U}, \boldsymbol{Ab}) \arrow[r, "\varinjlim"']
		& \boldsymbol{Ab}
	\end{tikzcd}
\end{center}
kommutiert. Damit ist der Beweis von Theorem~\ref{CechKohomologiegruppenFunktorURechtsableitung} abgeschlossen.

\end{proof}

\begin{bemerkung}[Übernommen aus \cite{tamme1975einfuhrung}, 1.2.2.8.]\label{CechKohomologieGruppenFunktorVonGarbenIstSchnittfunktor}
	Ist $F$ eine abelsche Garbe auf $\T$, so gilt
	\[
		\check{H}^{0}(U,F) = \underset{(U_{i} \rightarrow U)_{i \in I}}{\varinjlim} H^{0}((U_{i} \rightarrow U)_{i \in I}, F) = \underset{(U_{i} \rightarrow U)_{i \in I}}{\varinjlim} F(U) = F(U) \textrm{,}
	\]
	d.h. der Schnittfunktor $\Gamma_{U} : \S \rightarrow \Ab$ faktorisiert in
	\[
		\Gamma_{U} = \check{H}^{0}(U, \_) \circ \iota \textrm{,}
	\]
	wobei $\iota : \S \rightarrow \P$ die Inklusion bezeichnet.
\end{bemerkung}

 



\subsection{Die Funktoren $f^{\P}$ und $f_{\P}$}

\begin{festsetzung}
	In diesem Abschnitt sei neben  $\mathcal{T}$ ein weiterer Situs $\mathcal{T'}$ gegeben und $f : \mathcal{T} \rightarrow \mathcal{T'}$ bezeichne einen Funktor der zugrundeliegenden Kategorien ($f$ wird nicht notwendigerweise als Situs-Morphismus vorausgesetzt). Analog zu $\P$ bezeichne $\mathcal{P'}$ die Kategorie der abelschen Prägarben auf $\mathcal{T'}$.
\end{festsetzung}

\begin{definition}
	Für jeden Funktor $f : \mathcal{T} \rightarrow \mathcal{T'}$ definieren wir den kovarianten Funktor
	\[
		f^{\P} : \mathcal{P'} \rightarrow \mathcal{P}
	\]
	indem wir $f^{\P}F':=F' \circ f$ für jede abelsche Prägarbe $F'$ auf $\mathcal{T}$ und $f^{\P}v':=v' \circ f : f^{\P}F' \rightarrow f^{\P}G'$ für jeden Morphismus $v' : F' \rightarrow G'$ von Prägarben auf $\mathcal{T'}$ setzen.
\end{definition}

\begin{proposition}
	Der Funktor $f^{\P} : \mathcal{P'} \rightarrow \mathcal{P}$ ist additiv, exakt und kommutiert mit induktiven Limites.
\end{proposition}

\begin{proof}[Beweis (Ergänzt \cite{tamme1975einfuhrung}, 1.2.3.)]
	Die Additivität und Exaktheit folgen direkt aus der Definition von $f^{\P}$.
	Im Hinblick auf Proposition~\ref{InduktiverLimesVonFunktorenInPSindDarstellbar} ergibt sich für jeden Funktor $F : \mathcal{I} \rightarrow \mathcal{P'}$ und jedes $U \in \mathcal{T}$
	\begin{align*}
		\varinjlim(f^{\P} \circ F)(U) = \varinjlim (\Gamma_{U} \circ f^{\P} \circ F) = \varinjlim (\Gamma_{f(U)} \circ F) = f^{\P}(\varinjlim F)(U) \hspace{0.3em} .
	\end{align*}
\end{proof}

\begin{theorem}\label{ExistenzVonf_p} \quad
	\begin{enumerate}
		\item[(i)] Der zu $f^{\P} : \mathcal{P'} \rightarrow \mathcal{P}$ links-adjungierte Funktor $f_{\P} : \mathcal{P} \rightarrow \mathcal{P'}$ existiert, ist rechtsexakt, additiv und kommutiert mit induktiven Limites.
		
		\item[(ii)] Ist $f_{\P} : \mathcal{P} \rightarrow \mathcal{P'}$ exakt, so bildet $f^{\P} : \mathcal{P'} \rightarrow \mathcal{P}$ injektive Objekte aus $\mathcal{P'}$ auf injektive Objekte aus $\mathcal{P}$ ab.
	\end{enumerate}
\end{theorem}

Wir wollen im Folgenden den Funktor $f_{\P} : \P \rightarrow \P'$ nur konstruieren und für einen ausführlichen Beweis der Eigenschaften aus Theorem~\ref{ExistenzVonf_p} auf \cite{tamme1975einfuhrung} 1.2.3.1. verweisen.

\begin{proof}[Konstruktion von $f_{\P}$ (Basiert auf \cite{tamme1975einfuhrung}, 1.2.3.1.)]
	\renewcommand{\qedsymbol}{}
	Wir wollen zunächst für jede abelsche Prägarbe $F \in \P$ und jedes $U' \in \T'$ den Wert $f_{\P}F(U')$ konstruieren. Dazu definieren wir eine kleine Kategorie $\I_{U'}$: Die Objekte von $\I_{U'}$ sind alle Paare $(U, \phi')$, bestehend aus einem Objekt $U\in \T$ und einem Morphismus $\phi': U' \rightarrow f(U)$ in $\T'$. Ein Morphismus $(U_{1}, \phi_{1}') \rightarrow (U_{2}, \phi_{2}')$ in $\I_{U'}$ ist ein Morphismus $\phi : U_{1} \rightarrow U_{2}$, für den das Diagramm
	\begin{center}
		\shorthandoff{"}
		\begin{tikzcd}
			& U' \arrow[dl, "{\phi_{1}'}"'] \arrow[dr, "{\phi_{2}'}"]\\
			f(U_{1}) \arrow[rr, "f(\phi)"']
			&& f(U_{2})
		\end{tikzcd}
	\end{center}
	kommutiert. Wir erhalten dann einen kontravarianten Funktor
	\[
		F_{U'} : \I_{U'} \rightarrow \Ab \quad , \quad (U, \phi') \mapsto F(U)
	\]
	mit Werten in $\Ab$ und können daher dank Proposition~\ref{InduktiverLimesRechtsexakterFunktor}
	\[
		f_{\P}F(U'):= \underset{\mathcal{I}_{U'}^{op}}{\varinjlim} F_{U'} = \underset{(U, \phi')}{\varinjlim} F(U)
	\]
	setzen. Außerdem induziert ein Morphismus $\epsilon': U' \rightarrow V'$ in $\T'$ erst einen Funktor $\I_{V'} \rightarrow \I_{U'}$, indem wir dem Paar $(V, \phi' : V' \rightarrow f(V))$ das Paar $(V, \phi' \circ \epsilon': U' \rightarrow f(V))$ zuordnen und somit schließlich auch einen Morphismus
	\[
		\underset{\mathcal{I}_{V'}^{op}}{\varinjlim} F_{V'} \rightarrow \underset{\mathcal{I}_{U'}^{op}}{\varinjlim} F_{U'} \hspace{0.3em},
	\]
	d.h. $f_{\P}$ wird zu einer abelschen Prägarbe auf $\T'$.
\end{proof}

\begin{beispiel}\label{f_pBeiDarstellbarenPrägarben}
	In \cite{tamme1975einfuhrung} 1.2.3.3. befindet sich ein ausführlicher Beweis der folgenden Behauptung:
	Wird die abelsche Prägarbe $F$ auf $\T$ durch das Objekt $Z\in \T$ dargestellt, so wird die Prägarbe $f_{p}F$ durch das Objekt $f(Z)\in \T'$ repräsentiert.
\end{beispiel}

\begin{beispiel}[Übernommen aus \cite{tamme1975einfuhrung}, 1.2.3.4.]\label{Beipsielf^pUndf_pAufIndiskreterTopologieP}
	Wir bezeichnen mit $\boldsymbol{P}$ den indiskreten Situs, dessen zugrundeliegende Kategorie aus nur einem Objekt und nur einem Pfeil besteht. Die Kategorie der abelschen Prägarben auf $\boldsymbol{P}$ identifiziert sich mit $\Ab$. Zu jedem Objekt $U$ aus dem Situs $\T$ gibt es genau einen Funktor $i: \boldsymbol{P} \rightarrow \T$ der das einzige Objekt von $\boldsymbol{P}$ auf $U$ abbildet. Offenbar gilt
	\[
		i^{\P} : \P \rightarrow \Ab \quad ,\quad F \mapsto i^{\P}(F)=F(U) \hspace{0.3em}.
	\]
	Doch wie lässt sich der dazu links-adjungierte Funktor $i_{\P} : \Ab \rightarrow \P$ berechnen?
	Dazu sei eine abelsche Gruppe $A\in \Ab$ gegeben. Für jedes Objekt $V\in \T$ identifizieren sich die Objekte der Kategorie $\I_{V}$ (vgl. Konstruktion~\ref{ExistenzVonf_p}) mit den Morphismen $\phi : V \rightarrow U$ und für je zwei Morphismen $\phi, \psi: V \rightarrow U$ hat man
	\[
		Hom_{\I_{V}}(\phi, \psi)=
		\begin{cases}
			\emptyset & \textrm{, falls } \phi \neq \psi \\
			\{id_{\phi}\} & \textrm{, falls } \phi = \psi \hspace{0.3em}.
		\end{cases}
	\]
	Die Kategorie $\I_{V}$ identifiziert sich folglich mit der diskreten Kategorie auf der Menge $Hom_{\T}(V,U)$ und wir erhalten
	\[
		i_{\P}(A)= \underset{\mathcal{\I}_{V}^{op}}{\varinjlim} A = \underset{Hom_{\T}(V,U)}{\varinjlim} A = \bigoplus\limits_{Hom_{\T}(V,U)} A \hspace{0.3em}.
	\]
	
	Da die Bildung der direkten Summen abelscher Gruppen nach Theorem~\ref{InduktiverLimesExakterFunktor} ein exakter Funktor ist, erhalten wir aus Theorem~\ref{ExistenzVonf_p} (ii) : Ist $F$ eine injektive abelsche Garbe auf $\T$, so ist $F(U)$ für jedes $U\in \T$ eine injektive abelsche Gruppe.
	
	Wählt man speziell $\mathbb{Z}$ für die abelsche Gruppe $A$, so ist $i_{\P}(\mathbb{Z})$ gleich der Prägarbe $Z_{U}$ aus Bemerkung~\ref{GeneratorenInKategorieDerAbelschenPrägarben} und wir erhalten aus der Linksadjungiertheit von $i_{\P}$ zu $i^{\P}$ erneut die in $F \in \P$ funktoriellen Isomorphismen $Hom(\mathbb{Z}, F(U)) \cong Hom(Z_{U}, F) \hspace{0.3em}$.
\end{beispiel}
\section{Induktive Limites}
	
	\subsection{Induktive und projektive Limites}
	
	\begin{definition}\label{DarstellbareFunktorenDef}
		Sei $\mathcal{C}$ eine beliebige Kategorie und $F: \mathcal{C} \rightarrow \Set$ ein Funktor. Dann heißt $F$ \textit{darstellbar} oder auch \textit{repräsentierbar}, wenn ein Objekt $A \in \mathcal{C}$ und ein Isomorphismus $F \cong Hom(A,\_)$ existieren. Ist $F$ darstellbar, so ist das Objekt $A$ dank dem Yoneda-Lemma bis auf Isomorphie eindeutig bestimmt und wir nennen $A$ das \textit{darstellende Objekt} von $F$ bzw. sagen, dass $F$ durch $A$ \textit{dargestellt} wird.
	\end{definition}
	
	\begin{definition}
		Es seien $\mathcal{I}$ und $\mathcal{C}$ beliebige Kategorien. Für jedes $X \in \mathcal{C}$ bezeichnen wir mit 
		\[
			c_{X}: \mathcal{I} \rightarrow \mathcal{C} , \quad A \mapsto X , \quad f \mapsto id_{X}
		\]
		den \textit{konstanten Funktor} mit Wert $X$. Jeder Morphismus $f \in Hom_{\mathcal{C}}(X,Y)$ induziert in kanonischer Weise eine natürliche Transformation $c_{X} \rightarrow c_{Y}$.
	\end{definition}

	\begin{definition}
		Sei $\mathcal{I}$ eine kleine Kategorie, $\mathcal{C}$ eine beliebige Kategorie und $F: \mathcal{I} \rightarrow \mathcal{C}$ ein kovarianter Funktor. Dann erhält man einen kovarianten Funktor
		\[
			\varinjlim F: \mathcal{C} \rightarrow Set, \quad X \mapsto Hom(F, c_{X}),
		\]
		indem man jedem $X \in \mathcal{C}$ die Menge der natürlichen Transformationen $Hom(F, c_{X})$ zuordnet. Ist der Funktor $\varinjlim F$ darstellbar, so bezeichnet man das darstellende Objekt in $\mathcal{C}$ mit $\underset{i \in \mathcal{I}}{\varinjlim} F$ oder $\underset{\mathcal{I}}{\varinjlim} F$ und nennt es den \textit{induktiven Limes} von $F$. Weiter bezeichnet man die kleine Kategorie $\I$ als die \textit{Indexkategorie}.
		
		Dual hierzu erhält man einen kontravarianten Funktor
		\[
			\varprojlim F: \mathcal{C} \rightarrow Set, \quad X \mapsto Hom(c_{X}, F).
		\]
		Ist dieser darstellbar, so bezeichnet man das darstellende Objekt in $\mathcal{C}$ mit $\underset{i \in \mathcal{I}}{\varprojlim} F$ oder $\underset{\mathcal{I}}{\varprojlim} F$ und nennt es den \textit{projektiven Limes} von $F$.
	\end{definition}

	\begin{beispiel}[Inspiriert durch \cite{tamme1975einfuhrung}, 1.3.11.2.] \label{KoproduktAlsInduktiverLimesBeispiel}
		Es sei $(A_{i})_{i\in I}$ eine Familie von Objekten in der Kategorie $\C$. Wir fassen die Menge $I$ als diskrete Kategorie auf und definieren den Funktor $A : I \rightarrow \C , i \mapsto A_{i}$. Für jedes $T \in \C$ identifiziert sich die Menge $Hom(A, c_{T})$ in kanonischer Weise mit der Menge $\prod\limits_{i \in I}Hom_{\C}(A_{i}, T)$. Folglich ist der Funktor $\varinjlim A$ genau dann darstellbar, wenn das Koprodukt $\coprod\limits_{i \in I}A_{i}$ in $\C$ existiert. In diesem Fall gilt für den induktiven Limes $\underset{i \in I}{\varinjlim} A = \coprod\limits_{i \in I}A_{i}$. Analoges gilt für die Darstellbarkeit des projektiven Limesfunktor und die Existenz des Produktes $\prod\limits_{i \in I}A_{i}$ in $\C$.
	\end{beispiel}

	\begin{proposition}
		Sei $\mathcal{I}$ eine kleine und $\mathcal{C}$ eine beliebige Kategorie. Dann ist
		\[
			\varinjlim : Hom(\mathcal{I}, \mathcal{C}) \rightarrow Hom(\mathcal{C}, Set), \quad F \mapsto \varinjlim F
		\]
		ein kontravarianter Funktor.
	\end{proposition}
	
	\begin{bemerkung}[Ergänzt \cite{tamme1975einfuhrung}, 0.3.1.]
		Sind $\varinjlim F$ und $\varinjlim F'$ darstellbar, so entspricht eine natürliche Transformation $\varinjlim f : \varinjlim F' \rightarrow \varinjlim F$ nach dem Yoneda-Lemma einem eindeutig bestimmten Morphismus $\underset{i \in \mathcal{I}}{\varinjlim} f : \underset{i \in \mathcal{I}}{\varinjlim} F \rightarrow \underset{i \in \mathcal{I}}{\varinjlim} F'$ in $\mathcal{C}$.
	\end{bemerkung}
	
	\begin{proposition}\label{InduktiverLimesRechtsexakterFunktor}
		Ist $\mathcal{I}$ eine kleine Kategorie und $\mathcal{C}$ eine abelsche Kategorie mit der Eigenschaft (Ab3), dann ist $\varinjlim F$ für jeden Funktor $F: \mathcal{I} \rightarrow \mathcal{C}$ darstellbar und
		\[
			\underset{i \in \mathcal{I}}{\varinjlim} : Hom(\mathcal{I}, \mathcal{C}) \rightarrow \mathcal{C}, \quad F \mapsto \underset{i \in \mathcal{I}}{\varinjlim} F
		\]
		ist ein rechtsexakter, additiver Funktor abelscher Kategorien.
	\end{proposition}
	
	\begin{proof}[Beweis (Ergänzt \cite{tamme1975einfuhrung}, 0.3.1.1.)]
		Aufgrund von (Ab3) existiert die direkte Summe $S:= \bigoplus\limits_{i \in I} F(i)$ in $\C$ zusammen mit den Inklusionen $ins_{i} : F(i) \rightarrow S$ für alle $i \in \I$. Für jeden Morphismus $u : i \rightarrow j$ in $\I$ definieren wir nun $N_{u}:=Im(ins_{i}- ins_{j} \circ F(u))$. Da $\I$ eine kleine Kategorie ist, bildet die Gesamtheit der Morphismen in $\I$ eine Menge, welche wir mit $Mor_{\I}$ bezeichnen wollen. Wegen (Ab3) existiert nun auch die direkte Summe $\bigoplus\limits_{u \in Mor_{\I}} N_{u}$ zusammen mit den Inklusionen $ins_{u} : N_{u} \rightarrow \bigoplus\limits_{u \in Mor_{\I}} N_{u}$ für alle $u \in Mor_{\I}$. Die kanonischen Morphismen $N_{u} \rightarrow S$ induzieren hinsichtlich der universellen Eigenschaft von Koprodukten einen eindeutigen Morphismus $ \rho: \bigoplus\limits_{u \in Mor_{\I}} N_{u} \rightarrow S$. Wir wollen nun zeigen, dass der Quotient $\underset{i \in \I}{\varinjlim}F:=Coker(\rho)=S/Im(\rho)$ darstellendes Objekt des Funktors $\varinjlim$ ist.
		Dazu überlegen wir uns, dass für jeden Morphismus $\alpha: S \rightarrow X$ gilt:
		\begin{align*}
			&(\alpha \circ ins_{i} : F(i) \rightarrow X)_{i \in \I} \textrm{ ist eine natürliche Transformation } F \rightarrow c_{X} \\
			\Leftrightarrow & \forall u \in Mor_{\I}: \alpha \circ ins_{i}=\alpha \circ ins_{j} \circ F(u) \\
			\Leftrightarrow & \forall u \in Mor_{\I}: \alpha \circ (ins_{i} - ins_{j} \circ F(u)) = 0 \\
			\Leftrightarrow & \forall u \in Mor_{\I}: \alpha \circ im(ins_{i} - ins_{j} \circ F(u)) = 0 \\
			\Leftrightarrow  &\quad \alpha \circ \rho = 0 \\
			\Leftrightarrow & \exists! f_{\alpha} \in Hom_{\C}(Coker(\rho),X): \alpha = f_{\alpha} \circ coker(\rho)
		\end{align*}
		Daraus folgt, dass für alle $X \in \C$ ein Isomorphismus $Hom(\underset{i \in \I}{\varinjlim}F, X) \rightarrow Hom(F, c_{X})$ existiert und zwar derart, dass für alle Morphismen $X \rightarrow Y$ in $\C$ das induzierte Diagramm
		\begin{center}
			\shorthandoff{"}
			\begin{tikzcd}[column sep = small, row sep = small]
				Hom(\underset{i \in \I}{\varinjlim}F, X) \arrow[r] \arrow[d] & Hom(F, c_{X}) \arrow[d] \\
				Hom(\underset{i \in \I}{\varinjlim}F, Y) \arrow[r] & Hom(F, c_{Y})
			\end{tikzcd}
		\end{center}
		kommutiert.
		
		Ist nun $\beta: F \rightarrow G$ eine natürliche Transformation von Funktoren $F,G \in Hom(\I, \C)$, so bildet $(incl_{G(i)} \circ \beta(i): F(i) \rightarrow \underset{i \in \I}{\varinjlim}G)_{i \in \I}$ eine natürliche Transformation $F \rightarrow \underset{i \in \I}{\varinjlim}G$, wobei $incl_{G(i)}$ den kanonischen Morphismus $ G(i) \xrightarrow{ins_{i}} \bigoplus\limits_{i \in \I} G(i) \rightarrow \underset{i \in \I}{\varinjlim}G$ für jedes $i\in \I$ bezeichnet. Es existiert genau ein Morphismus $\alpha: S \rightarrow \underset{i \in \I}{\varinjlim}G$, für welchen $\alpha \circ ins_{i}= incl_{G(i)} \circ \beta$ gilt. Der Morphismus $\underset{i \in \mathcal{I}}{\varinjlim}(\beta)$ ist nun gleich dem Morphismus $f_{\alpha} \in Hom_{\C}(\underset{i \in \I}{\varinjlim}F,\underset{i \in \I}{\varinjlim}G)$, welcher dank der obenstehenden Äquivalenzumformungen existiert. Anhand der Herleitung des Morphismus $\underset{i \in \mathcal{I}}{\varinjlim}(\beta)$ lässt sich leicht überprüfen, dass $\underset{i \in \mathcal{I}}{\varinjlim}$ tatsächlich einen additiven Funktor bildet.
		
		Sei nun $0 \rightarrow F \xrightarrow{\alpha} G \xrightarrow{\beta} H \rightarrow 0$ eine kurze exakte Sequenz in $Hom(\I, \C)$. Wir wollen nun zeigen, dass auch die Sequenz
		\[
			\underset{i \in \I}{\varinjlim}F \xrightarrow{\underset{i \in \mathcal{I}}{\varinjlim}(\alpha)} \underset{i \in \I}{\varinjlim}G \xrightarrow{\underset{i \in \mathcal{I}}{\varinjlim}(\beta)} \underset{i \in \I}{\varinjlim}H \rightarrow 0
		\]
		der induktiven Limites exakt ist. Laut Voraussetzung ist auch $0 \rightarrow F(i) \xrightarrow{\alpha} G(i) \xrightarrow{\beta} H(i) \rightarrow 0$ für alle $i \in \I$ exakt. Zusammen mit der obenstehenden Konstruktion von $\underset{i \in \mathcal{I}}{\varinjlim}(\beta)$ folgt, dass für jedes $X \in \C$ das Diagramm
		\begin{equation}\label{BewInduktiverLimesRechtsexaktKomDiag1}
			\shorthandoff{"}
			\begin{tikzcd}
				Hom(\underset{i \in \I}{\varinjlim}H, X) \arrow[d, "{Hom(\_,X)(\underset{i \in \mathcal{I}}{\varinjlim}(\beta))}"'] \arrow[r, hook]
				& Hom(\bigoplus\limits_{i \in \I} H(i), X) \arrow[r, "\cong"]
				& \prod\limits_{i \in \I}Hom(H(i), X) \arrow[d, hook] \\
				Hom(\underset{i \in \I}{\varinjlim}G, X) \arrow[r, hook]
				& Hom(\bigoplus\limits_{i \in \I} G(i), X) \arrow[r, "\cong"]
				& \prod\limits_{i \in \I}Hom(G(i), X)
			\end{tikzcd} \tag{$\star$}
		\end{equation}
		kommutiert. Daraus folgt, dass der Morphismus $\underset{i \in \mathcal{I}}{\varinjlim}(\beta)$ ein Epimorphismus ist. Weiter gilt
		\begin{align*}
			0=\underset{i \in \mathcal{I}}{\varinjlim}(0)= \underset{i \in \mathcal{I}}{\varinjlim}(\beta \circ \alpha) = \underset{i \in \mathcal{I}}{\varinjlim}(\beta) \circ \underset{i \in \mathcal{I}}{\varinjlim}(\alpha)\textrm{,}
		\end{align*}
		weshalb nur noch $Ker(\underset{i \in \mathcal{I}}{\varinjlim}\beta)\subseteq Im(\underset{i \in \mathcal{I}}{\varinjlim}\alpha)$ zu zeigen bleibt.
		Unter Berücksichtigung von Korollar~\ref{ExaktheitÄquivalenteDefinition} erhalten wir analog zu den obenstehenden Konstruktionen für alle Morphismen $i\rightarrow j$ in $\I$ das kommutative Diagramm
		\begin{center}
			\shorthandoff{"}
			\begin{tikzcd}[column sep=small, row sep= small]
			F(i) \arrow[rr, hook, "\alpha(i)"] \arrow[dd] \arrow[dr]
			&& G(i) \arrow[rr, two heads] \arrow[dd] \arrow[dr]
			&& Coker(\alpha(i)) \arrow[rr, "\cong"] \arrow[dd] \arrow[dr]
			&& H(i) \arrow[dd] \arrow[dr] \\
			& \underset{i \in \mathcal{I}}{\varinjlim} F \arrow[rr, crossing over, "\underset{i \in \mathcal{I}}{\varinjlim} \alpha" near start]
			&& \underset{i \in \mathcal{I}}{\varinjlim} G \arrow[rr, two heads, crossing over]
			&& \underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha)) \arrow[rr, "\cong" near start, crossing over]
			&& \underset{i \in \mathcal{I}}{\varinjlim} H \\
			F(j) \arrow[rr, hook, "\alpha(j)"'] \arrow[ur]
			&& G(j) \arrow[rr, two heads] \arrow[ur]
			&& Coker(\alpha(j)) \arrow[rr, "\cong"] \arrow[ur]
			&& H(j) \arrow[ur]
			& \hspace{0.3em} ,
			\end{tikzcd}
		\end{center}
		wobei $Coker(\alpha) \in Hom(\I, \C)$ den Funktor bezeichnet, welcher jedem Objekt $i\in \I$ den Kokern $Coker(\alpha(i))$ und jedem Morphismus $i\rightarrow j$ in $\I$ den von der natürlichen Transformation $\alpha$ induzierten Morphismus $Coker(\alpha(i))\rightarrow Coker(\alpha(j))$ zuweist.
		Hinsichtlich Korollar~\ref{ExaktheitÄquivalenteDefinition} reicht es demnach zu zeigen, dass $\underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha))= Coker(\underset{i \in \mathcal{I}}{\varinjlim} \alpha)$ gilt. Tatsächlich erfüllt $\underset{i \in \mathcal{I}}{\varinjlim} (Coker (\alpha))$ die universelle Eigenschaft des Kokerns von $\underset{i \in \mathcal{I}}{\varinjlim} \alpha$, denn für alle Morphismen $u: \underset{i \in \mathcal{I}}{\varinjlim} G \rightarrow X$ in $\C$ gilt:
		\begin{align*}
			&u \circ \underset{i \in \mathcal{I}}{\varinjlim} \alpha=0 \\
			\Leftrightarrow & \forall i \in \I : u \circ \underset{i \in \mathcal{I}}{\varinjlim} \alpha \circ incl_{F(i)}=0 \\
			\Leftrightarrow & \forall i \in \I : u \circ incl_{G(i)} \circ \alpha(i)=0 \\
			\Leftrightarrow & \forall i \in \I \exists! v_{i}\in Hom(Coker(\alpha(i)),X) : u \circ incl_{G(i)} = v_{i} \circ coker(\alpha(i)) \\
			\Leftrightarrow & \exists! v \in Hom(\underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha)),X) \forall i \in \I : u \circ incl_{G(i)} = v \circ incl_{Coker(\alpha(i))} \circ coker(\alpha(i)) \\
			\Leftrightarrow & \exists! v \in Hom(\underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha)),X) \forall i \in \I : u \circ incl_{G(i)} = v \circ \underset{i \in \mathcal{I}}{\varinjlim}(coker(\alpha)) \circ incl_{G(i)}\\
			\Leftrightarrow & \exists! v \in Hom(\underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha)),X) : u = v \circ \underset{i \in \mathcal{I}}{\varinjlim}(coker(\alpha))
		\end{align*}
		Dabei bezeichnet $\underset{i \in \mathcal{I}}{\varinjlim}(coker(\alpha))$ den Morphismus $\underset{i \in \mathcal{I}}{\varinjlim} G \rightarrow \underset{i \in \mathcal{I}}{\varinjlim} (Coker(\alpha))$, welcher von der natürlichen Transformation $(incl_{Coker(\alpha(i))} \circ coker(\alpha(i)))_{i \in \I}$ zwischen den Funktoren $G$ und $c_{\underset{i \in \mathcal{I}}{\varinjlim}(Coker(\alpha))}$ induziert wird. Man sieht mit Hilfe eines kommutativen Diagramms analog zu Diagramm~(\ref{BewInduktiverLimesRechtsexaktKomDiag1}), dass $\underset{i \in \mathcal{I}}{\varinjlim}(coker(\alpha))$ ein Epimorphismus ist.
	\end{proof}
	
	\begin{proposition}\label{VertauschbarkeitVonLimesUndAdjunktion}
		Sei $\mathcal{I}$ eine kleine Kategorie und $F: \mathcal{D} \rightarrow \mathcal{C}$ ein Funktor, zu welchem der links-adjungierte Funktor $^{ad}F: \mathcal{C} \rightarrow \mathcal{D}$ existiert. Dann kommutiert $F$ mit darstellbaren projektiven und $^{ad}F$ mit darstellbaren induktiven Limites.
		
		Konkret bedeutet das für $^{ad}F$ (und entsprechend für $F$): Ist $G: \mathcal{I} \rightarrow \mathcal{C}$ ein Funktor mit darstellbarem induktiven Limes, so ist auch der induktive Limes von ${}^{ad}F \circ G$ darstellbar und es gilt
		\[
			\underset{i \in \mathcal{I}}{\varinjlim} (^{ad}F \circ G) = {}^{ad}F(\underset{i \in \mathcal{I}}{\varinjlim} G)
		\]
		bis auf Isomorphie (vgl. Def.~\ref{DarstellbareFunktorenDef}).
	\end{proposition}
	
\begin{proof}[Beweis (Eigenleistung)]
	Für beliebige $X \in \mathcal{D}$ gilt per Definition von Adjunktion und von Darstellbarkeit
	\setcounter{equation}{0}
	\begin{align}
		Hom({}^{ad}F(\underset{i \in \mathcal{I}}{\varinjlim} G), X) \cong Hom(\underset{i \in \mathcal{I}}{\varinjlim} G, F(X)) \cong Hom(G, c_{F(X)}) \hspace{0.3em} .
	\end{align}
	Aus der Definition des konstanten Funktors geht hervor, dass $c_{F(X)} = F \circ c_{X}$ gilt. Da $^{ad}F$ links-adjungiert zu $F$ ist, gilt also weiter
	\begin{align}
		Hom({}^{ad}F \circ G, c_{X}) \cong Hom(G, F \circ c_{X}) = Hom(G, c_{F(X)}) \hspace{0.3em} .
	\end{align}
	Wegen (1) und (2) ist ${}^{ad}F(\underset{i \in \mathcal{I}}{\varinjlim} G)$ darstellendes Objekt des induktiven Limes von ${}^{ad}F \circ G$. 
\end{proof}
	
	
	
	
	\subsection{Exaktheit induktiver Limites}
	
	\begin{definition}
		Eine Kategorie $\mathcal{I}$ heißt \textit{pseudofiltrierend}, wenn sie die folgenden beiden Eigenschaften besitzt:
		\begin{enumerate}
			\item[(PS1)] Jedes Diagramm der Form
			\begin{center}
				\shorthandoff{"}
				\begin{tikzcd}[row sep=tiny]
					& j \\
					i \arrow[ur] \arrow[dr] && \hspace{0.25em}\\
					& j'
				\end{tikzcd}
			\end{center}
			in $\mathcal{I}$ lässt sich zu einem kommutativen Diagramm der Form
			\begin{center}
				\shorthandoff{"}
				\begin{tikzcd}[row sep=tiny]
				& j \arrow[dr] \\
				i \arrow[ur] \arrow[dr] 
				&& k\\
				& j' \arrow[ur]
				\end{tikzcd}
			\end{center}
			ergänzen.
			
			\item[(PS2)] Jedes Diagramm der Form
			\begin{center}
				\shorthandoff{"}
				\begin{tikzcd}
					i \arrow[r, shift left= 1, "u"] \arrow[r, shift right= 1, "v"']& j & \hspace{0.4em}
				\end{tikzcd}
			\end{center}
			in $\mathcal{I}$ lässt sich zu einem Diagramm  der Form
			\begin{center}
				\shorthandoff{"}
				\begin{tikzcd}
				i \arrow[r, shift left= 1, "u"] \arrow[r, shift right= 1, "v"']& j \arrow[r, "w"] & k
				\end{tikzcd}
			\end{center}
		mit $wu=wv$ ergänzen.
		\end{enumerate}
	\end{definition}

	\begin{definition}
		Eine nicht-leere Kategorie $\mathcal{I}$ heißt \textit{filtrierend}, wenn sie pseudofiltrierend ist und zu je zwei Objekten $i,j \in \mathcal{I}$ ein Objekt $k \in \mathcal{I}$ und Morphismen $i \rightarrow k$ und $j \rightarrow k$ existieren.
	\end{definition}

	\begin{beispiel}[Eigenleistung]\label{GerichteteMengeAlsFiltrierendeKategorie}
		Wir können jede gerichtete Menge $X$ (vgl. Definition~\ref{GerichteteMengeDefinition}) als filtrierende Kategorie auffassen, indem wir $X$ als die Menge der Objekte festlegen und die Relation $\geq$ als die Menge der Morphismen interpretieren. D.h. es existiert genau dann ein Morphismus $x \rightarrow y$ zwischen Objekten $x,y \in X$, wenn $y \geq x$ gilt und in diesem Falle auch nur genau ein Morphismus von $x$ in $y$.
		
		Diese Auffassung einer gerichteten Menge gibt uns auch eine Möglichkeit eine aufsteigend filtrierende Familie $(A_{i})_{i \in I}$ (vgl. Definition~\ref{AufsteigendFiltrierendeFamilieVonUnterobjektenDef}) von Unterobjekten eines Objektes $A$ in einer Kategorie $\mathcal{C}$ als filtrierende Kategorie zu interpretieren.
	\end{beispiel}

	\begin{theorem}\label{InduktiverLimesExakterFunktor}
		Ist $\mathcal{I}$ eine kleine pseudofiltrierende Kategorie und $\mathcal{C}$ eine abelsche Kategorie mit der Eigenschaft (Ab5), dann ist der Funktor
		\[
			\varinjlim : Hom(\mathcal{I}, \mathcal{C}) \rightarrow \mathcal{C}
		\]
		exakt.
	\end{theorem}

	Für einen ausführlichen Beweis von Theorem~\ref{InduktiverLimesExakterFunktor} vergleiche man \cite{schubert1970band}, 14.6.6.
	
	
	
	
	
	
	\subsection{Finale Unterkategorien}
	
	\begin{definition}
		Sei $\mathcal{I}$ eine Kategorie. Eine Unterkategorie $\mathcal{J}$ von $\mathcal{I}$ heißt \textit{final}, wenn $\mathcal{J}$ eine volle Unterkategorie von $\mathcal{I}$ ist und für alle $i \in \mathcal{I}$ ein Morphismus $i \rightarrow j$ mit $j \in \mathcal{J}$ existiert.
	\end{definition}

	\begin{beispiel}
		Besitzt die Kategorie $\mathcal{I}$ ein finales Objekt $\infty$, so ist $\mathcal{J}:=(\{\infty\}, \{id_{\infty}\})$ eine finale Unterkategorie von $\mathcal{I}$.
	\end{beispiel}

	\begin{festsetzung}
		Ist $F: \mathcal{I} \rightarrow \mathcal{C}$ ein Funktor zwischen den Kategorien $\mathcal{I}$ und $\mathcal{C}$, so bezeichnen wir für jede Unterkategorie $\mathcal{J}$ von $\mathcal{I}$ mit $F \vert_{\mathcal{J}}$ die Einschränkung von $F$ auf $\mathcal{J}$.
	\end{festsetzung}
	
	\begin{proposition}\label{InduktiverLimesEinschränkungAufFinaleUnterkategorie}
		Ist $\mathcal{I}$ eine kleine Kategorie mit der Eigenschaft (PS1), so ist die kanonische natürliche Transformation
		\[
			\varinjlim F \rightarrow \varinjlim (F \vert_{\mathcal{J}})
		\]
		für jede finale Unterkategorie $\mathcal{J}$ von $\mathcal{I}$ ein Isomorphismus.
	\end{proposition}
	
	Für den Beweis von Proposition~\ref{InduktiverLimesEinschränkungAufFinaleUnterkategorie} vergleiche man \cite{tamme1975einfuhrung} Kapitel 0, Proposition 3.3.1.\\
	
	Aus Proposition~\ref{InduktiverLimesEinschränkungAufFinaleUnterkategorie} folgt insbesondere:
	
	\begin{korollar}
		Besitzt die kleine Kategorie $\mathcal{I}$ ein finales Objekt $\infty$, so ist $\varinjlim F$ für jeden Funktor $F: \mathcal{I} \rightarrow \mathcal{C}$ darstellbar und es gilt $\underset{i \in \mathcal{I}}{\varinjlim} F \cong F(\infty)$.
	\end{korollar}